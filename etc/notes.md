# Weight and Balance

| Aircraft              | MTOW           ||
|:----------------------|-------:|-------:|
| Eurofox 3K            | 560kg  | 1234lb |
| Aquila A210           | 750kg  | 1653lb |
| Cessna 152            | 757kg  | 1670lb |
| Cessna 172R           | 1111kg | 2450lb |
| Cessna 182P           | 1338kg | 2950lb |
| 8KCAB Super Decathlon | 884kg  | 1950lb |

----

* BEW = Basic Empty Weight
* ZFW = Zero Fuel Weight
* MTOW = Maximum Take-off Weight
* MRW = Maximum Ramp Weight
* MLW = Maximum Landing Weight

----

* Moment = Weight x Arm
* Arm = Moment ÷ Weight
* Weight = Moment ÷ Arm

----

* Do NOT take-off outside Weight/Balance limitations

  **Civil Aviation Order (CAO) 20.7.4**

* Ensure all baggage is properly restrained

  **Civil Aviation Order 20.16.2**

----

* All Flightscope Aviation aircraft flight manuals can be found on the website under **Student Resources**
