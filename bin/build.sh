#!/bin/sh

dist_dir="public"
etc_dir="etc"
handout_dir="${dist_dir}/handout"

slides() {
  (cd revealjs-template && sh ./bin/build.sh ../${dist_dir} ../src)
}

externalImages() {
  mkdir -p "${dist_dir}/images"

  wget "https://presentation-slides-media.gitlab.io/aip-excerpt/general-conversions.png" -O "${dist_dir}/images/general-conversions.png"
  wget "https://presentation-slides-media.gitlab.io/aip-excerpt/fuel-weight-conversion.png" -O "${dist_dir}/images/fuel-weight-conversion.png"
}

makePdf() {
  mkdir -p "${handout_dir}"

  convert "${dist_dir}/images/general-conversions.png" "${handout_dir}/general-conversions.pdf"
  convert "${dist_dir}/images/fuel-weight-conversion.png" "${handout_dir}/fuel-weight-conversion.pdf"
  convert "${etc_dir}/cessna-172-loading-graph.png" "${handout_dir}/cessna-172-loading-graph.pdf"
  convert "${etc_dir}/cessna-172r-cg-moment-envelope.png" "${handout_dir}/cessna-172r-cg-moment-envelope.pdf"
  convert "${etc_dir}/cessna-172s-cg-moment-envelope.png" "${handout_dir}/cessna-172s-cg-moment-envelope.pdf"
  convert "${etc_dir}/cessna-152-loading-graph.png" "${handout_dir}/cessna-152-loading-graph.pdf"
  convert "${etc_dir}/cessna-152-cg-moment-envelope.png" "${handout_dir}/cessna-152-cg-moment-envelope.pdf"
  convert "${etc_dir}/eurofox-wb.drawio.png" "${handout_dir}/eurofox-wb.drawio.pdf"
  convert "${etc_dir}/cessna-172-wb.drawio.png" "${handout_dir}/cessna-172-wb.drawio.pdf"
  convert "${etc_dir}/cessna-152-wb.drawio.png" "${handout_dir}/cessna-152-wb.drawio.pdf"
  libreoffice --invisible --headless --convert-to pdf "${etc_dir}/cover.fodt" --outdir "${handout_dir}"
}

makeNotes() {
  # pandoc --highlight-style=espresso -fmarkdown-implicit_figures -M mainfont="DejaVu Sans Mono" --pdf-engine=xelatex "${etc_dir}/notes.md" -o ${handout_dir}/notes.pdf
  pandoc --highlight-style=espresso -fmarkdown-implicit_figures -M mainfont="DejaVu Sans Mono" "${etc_dir}/notes.md" -o ${handout_dir}/notes.pdf
}

makeHandout() {
  pdftk \
    "${handout_dir}/cover.pdf" \
    "${handout_dir}/general-conversions.pdf" \
    "${handout_dir}/fuel-weight-conversion.pdf" \
    "${handout_dir}/cessna-172-loading-graph.pdf" \
    "${handout_dir}/cessna-172r-cg-moment-envelope.pdf" \
    "${handout_dir}/cessna-172s-cg-moment-envelope.pdf" \
    "${handout_dir}/cessna-152-loading-graph.pdf" \
    "${handout_dir}/cessna-152-cg-moment-envelope.pdf" \
    "${handout_dir}/eurofox-wb.drawio.pdf" \
    "${handout_dir}/eurofox-wb.drawio.pdf" \
    "${handout_dir}/cessna-172-wb.drawio.pdf" \
    "${handout_dir}/cessna-172-wb.drawio.pdf" \
    "${handout_dir}/cessna-152-wb.drawio.pdf" \
    cat output \
    "${handout_dir}/handout.pdf"
}

cessna172r() {
  pdftk \
    "${handout_dir}/general-conversions.pdf" \
    "${handout_dir}/fuel-weight-conversion.pdf" \
    "${handout_dir}/cessna-172-loading-graph.pdf" \
    "${handout_dir}/cessna-172r-cg-moment-envelope.pdf" \
    "${handout_dir}/cessna-172-wb.drawio.pdf" \
    cat output \
    "${handout_dir}/cessna-172r.pdf"
}

cessna172s() {
  pdftk \
    "${handout_dir}/general-conversions.pdf" \
    "${handout_dir}/fuel-weight-conversion.pdf" \
    "${handout_dir}/cessna-172-loading-graph.pdf" \
    "${handout_dir}/cessna-172s-cg-moment-envelope.pdf" \
    "${handout_dir}/cessna-172-wb.drawio.pdf" \
    cat output \
    "${handout_dir}/cessna-172s.pdf"
}

cessna152() {
  pdftk \
    "${handout_dir}/general-conversions.pdf" \
    "${handout_dir}/fuel-weight-conversion.pdf" \
    "${handout_dir}/cessna-152-loading-graph.pdf" \
    "${handout_dir}/cessna-152-cg-moment-envelope.pdf" \
    "${handout_dir}/cessna-152-wb.drawio.pdf" \
    cat output \
    "${handout_dir}/cessna-152.pdf"
}

eurofox() {
  pdftk \
    "${handout_dir}/general-conversions.pdf" \
    "${handout_dir}/fuel-weight-conversion.pdf" \
    "${handout_dir}/eurofox-wb.drawio.pdf" \
    cat output \
    "${handout_dir}/eurofox.pdf"
}

slides
externalImages
makePdf
makeNotes
makeHandout
cessna172r
cessna172s
cessna152
eurofox
