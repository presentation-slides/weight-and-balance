## Cessna 172R

* We will do another example for the Cessna 172R
* This is a little more complex but the same general principles apply
* We have 2 rows for PAX, 2 baggage areas
* There is also a _"utility category"_ within which certain additional operations are permitted
* The Cessna 172R also has a Maximum Ramp Weight (MRW)

---

## Cessna 172R

###### Maximum Ramp Weight

<div style="text-align: center; color: white; background-color: #2b2d2f">
  <span style="padding: 5px; font-weight: bold">The maximum allowed weight for take off plus the fuel burned during taxi and run up</span>
</div>

---

## Cessna 172R

###### Certificated Weights

<div style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-certificated-weights.png" alt="Cessna 172R Certificated Weights" width="500px" style="border: 4px solid #555"/>
</div>

---

## Cessna 172R

###### Baggage Limits

<div style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-baggage-weights.png" alt="Cessna 172R Baggage Limits" width="500px" style="border: 4px solid #555"/>
</div>

---

## Cessna 172R

###### Load Factor Limits

<div style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-normal-lf-limits.png" alt="Cessna 172R Normal Load Factor Limits" width="500px" style="border: 4px solid #555"/>
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-utility-lf-limits.png" alt="Cessna 172R Utility Load Factor Limits" width="500px" style="border: 4px solid #555"/>
</div>

---

## Cessna 172R

###### Stations

<div style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-stations.png" alt="Cessna 172R Stations" width="500px" style="border: 4px solid #555"/>
</div>

---

