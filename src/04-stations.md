## Stations

* Each aircraft will have various _"stations"_
* Stations can be thought of loosely as, _places we can put stuff in an empty aircraft_

---

## Stations

###### For example

* Flight crew station
* Fuel station
* Baggage area station
* Oil/fluids station
* Some aircraft have more than one of these and each are their own station
  * Row 1 crew
  * Row 2 crew
  * Forward fuel tank
  * Baggage areas A and B

---

