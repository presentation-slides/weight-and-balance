## Further Reading

* Your Basic Aeronautical Knowledge (BAK) text book discusses Weight and Balance
* **All** CASA flight crew licence exams (RPL/PPL/CPL/ATPL) include Weight and Balance questions
  * The sample aircraft in the exams are freely available
  * Search for _"CASA RPL,PPL & CPL Workbook"_

---

