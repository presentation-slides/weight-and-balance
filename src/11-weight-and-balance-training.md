## Weight and Balance throughout your training

* What you have seen is one, reliable method of calculating weight and balance
* You may have a slight variation on this method, and that is OK &hellip;
* &hellip; as long as you consistently arrive at the **correct** answer

---

## Weight and Balance throughout your training

* Some aircraft types are easier than others to exceed weight and/or CG limitations
* For example:
  * two 85kg adults in 24-8881, with full fuel (86L) and baggage (20 kg) is within all limits
  * however, VH-NKJ (Cessna 152), with two 85kg adults and maximum baggage (54kg), with **NO FUEL** is already over MTOW

---

## Weight and Balance throughout your training

* RA-Aus students training in Group A aircraft (MTOW &leq; 600kg) may be aware of the upcoming Group G aircraft category
* The MTOW of Group G aircraft is 760 kg
* However, many of these aircraft types will _very easily_ exceed weight and/or CG limitations
* Many Group A aircraft types easily exceed limits e.g. Sling 2
* It is important to understand Weight and Balance, regardless of the aircraft type that you train in

---

