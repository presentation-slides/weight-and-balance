## Empty Aircraft

* Each **individual** aircraft will have a specified empty weight in the flight manual
* This station is called the aircraft Basic Empty Weight (BEW)

---

## Empty Aircraft

<p style="text-align: center; font-size: large">
  <span style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">
    Eurofox 3K 24-4844
  </span>
</p>

<p style="text-align: center; font-size: large">
  Basic Empty Weight:
  <code style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">
    288.5 kg
  </code>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-4844-bew.png" alt="Eurofox 4844 BEW" width="550px" style="border: 4px solid #555"/>
</p>

---

## Empty Aircraft

<p style="text-align: center; font-size: large">
  <span style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">
    Eurofox 3K 24-8881
  </span>
</p>

<p style="text-align: center; font-size: large">
  Basic Empty Weight:
  <code style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">
    303.8 kg
  </code>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-bew.png" alt="Eurofox 8881 BEW" width="550px" style="border: 4px solid #555"/>
</p>

---

## Empty Aircraft

<p style="text-align: center; font-size: large">
  <span style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">
    Cessna 172R VH-SCN
  </span>
</p>

<p style="text-align: center; font-size: large">
  Basic Empty Weight:
  <code style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">
    761.8 kg
  </code>
  or
  <code style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">
    1679.4 lb
  </code>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna-172r-scn-bew.png" alt="Cessna 172R VH-SCN BEW" width="550px" style="border: 4px solid #555"/>
</p>

---

