| Weight | Balance |
|:------:|:-------:|
| <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/aeroplane-scales.png" alt="Aeroplane on Scales" width="200px"/> | <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/aeroplane-balance-pyramid.png" alt="Aeroplane Balance on Pyramid" width="350px"/> |

---

## Weight and Balance

###### Materials

* You will need
  * Pen/Pencil/Eraser/Sharpener _(erasable pen recommended)_
  * Ruler
  * Basic Calculator _(or phone calculator)_
* Provided
  * AIP GEN 2.6 Conversion Tables
  * W/B Calculation Grid for Cessna 172R, Cessna 172S, Cessna 152 and Eurofox 3K
  * CG Envelope for Cessna 172R and Cessna 172S
  * Loading Graph for Cessna 172 and Cessna 152
  * [Link to handout](handout/handout.pdf)
    * [Cessna 172R](handout/cessna-172r.pdf)
    * [Cessna 172S](handout/cessna-172s.pdf)
    * [Cessna 152](handout/cessna-152.pdf)
    * [Eurofox 3K](handout/eurofox.pdf)

---

## Weight and Balance

###### Aims and Objectives

* To explore & internalise the general principles of Weight and Balance applied to fixed-wing aircraft
* Apply the principles of Weight and Balance to both simple and complex aircraft types
* Perform a Weight and Balance calculation for typical training aircraft
* To gain knowledge of legislative requirements for Weight and Balance
* Develop a strategy to reliably and efficiently calculate Weight and Balance

---

## Weight and Balance

* It is an offence to take-off in an aeroplane outside of weight/balance limitations
* See Civil Aviation Order (CAO) 20.7.4
* You, Pilot in Command, must calculate these limitations before **every** flight
* Perhaps you have not seen your instructor calculate weight and balance before your flight
* Please be assured, **it has been done**

---


## Weight _then_ Balance

* A weight limitation calculation is separate to a balance calculation
* However, a balance calculation _depends_ on the result of a weight limitation calculation
* Therefore, we do a weight limitation calculation _first_
* A "balance calculation" is also sometimes called a CG calculation

---

## Types of weight limitations

| Name                            | Abbr |
|:--------------------------------|:-----|
| Maximum Take-off Weight         | MTOW |
| Maximum Ramp Weight             | MRW  |
| Maximum Landing Weight          | MLW  |
| Minimum Crew/Seat Weight        |      |
| Maximum Crew/Seat Weight        |      |
| Maximum Baggage Weight          |      |
| Maximum Floor Loading Intensity |      |

---

## Where to find weight limitations?

###### According to CASR1998

* For aircraft that are not type-certificated
  * the certificate of airworthiness for the aircraft
  * or if not specified in the certificate of airworthiness, the aircraft flight manual

---

