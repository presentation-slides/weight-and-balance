## Maximum Take-off Weight (MTOW)

* You will find the MTOW in the flight manual
* It is an **offence** to take-off in an aircraft outside of weight limitations
* It is also extremely dangerous

---

## Maximum Take-off Weight (MTOW)

###### Examples

| Aircraft              | MTOW           ||
|:----------------------|-------:|-------:|
| Eurofox 3K            | 560kg  | 1234lb |
| Aquila A210           | 750kg  | 1653lb |
| Cessna 152            | 757kg  | 1670lb |
| Cessna 172R           | 1111kg | 2450lb |
| Cessna 182P           | 1338kg | 2950lb |
| 8KCAB Super Decathlon | 884kg  | 1950lb |

---

## Maximum Take-off Weight (MTOW)

###### Example Flight Manual Eurofox 3K 24-8881

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-mtow.png" alt="Eurofox 8881 MTOW" width="550px" style="border: 4px solid #555"/>
</p>

<p style="text-align: center; font-size: large">
  <i>Important Note: Empty weight is attached to the flight manual as a supplement for each individual aircraft (non-standard)</i>
</p>

---

## Maximum Take-off Weight (MTOW)

###### Example Flight Manual Cessna 172R

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna-172r-mtow.png" alt="Cessna 172R MTOW" width="550px" style="border: 4px solid #555"/>
</p>

---

