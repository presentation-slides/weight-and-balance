## Calculating Weight Limitations

* Let's calculate weight limitations for a given scenario
* We will use the _same general technique_ for any given scenario
* Our scenario:
  * Eurofox 24-8881
  * Me (80kg) and Bob (80kg)
  * Full fuel (86L)
  * 10 kg Baggage

---

## Calculating Weight Limitations

* Draw a grid
* One row per station _(including aircraft BEW)_
* One column per Unit of Measurement
* Leave room for more columns, since we will use these for calculating balance

---

## Calculating Weight Limitations

###### Eurofox 3K Stations

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-stations.png" alt="Eurofox 3K Stations" width="550px"/>
</p>

---

## Calculating Weight Limitations

###### Draw the Grid

* An extra row & column for heading information
* Put the Fuel station **last**
* Leave an extra empty row before the Fuel station
* You'll see why soon

---

## Calculating Weight Limitations

###### Draw the Grid

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-eurofox.drawio.svg" alt="Eurofox 3K Weight Grid" width="600px"/>
</p>

---

## Calculating Weight Limitations

* Enter initial known values
* Ensure the values are under the **correct unit of measurement**

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-eurofox2.drawio.svg" alt="Eurofox 3K Weight Grid" width="600px"/>
</p>

---

## Calculating Weight Limitations

* The blank line is for a subtotal value
* This subtotal will be the aircraft with all stations loaded *except for Fuel*
* This station is called Zero Fuel Weight (ZFW)

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-eurofox3.drawio.svg" alt="Eurofox 3K Weight Grid" width="600px"/>
</p>

---

## Calculating Weight Limitations

* Using the conversion tables in AIP GEN 2.6, convert all stations to kilograms
* Litres to kilograms: multiply by `0.72`

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-eurofox4.drawio.svg" alt="Eurofox 3K Weight Grid" width="600px"/>
</p>

---

## Calculating Weight Limitations

* Compute the total for Zero Fuel Weight
* Compute the total including fuel _(called All Up Weight)_

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-eurofox5.drawio.svg" alt="Eurofox 3K Weight Grid" width="600px"/>
</p>

---

## Calculating Weight Limitations

* **Verify we are within all weight limitations**
* MTOW: `560 kg`
* MLW: `560 kg`
* Maximum Baggage: `20 kg`
* Minimum Crew Weight: `54 kg`

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-eurofox5.drawio.svg" alt="Eurofox 3K Weight Grid" width="600px"/>
</p>

---

## Calculating Weight Limitations

###### We are within **weight** limitations

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/tick.png" alt="Tick" width="20px"/>
  <br/>
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/aeroplane-scales.png" alt="Aeroplane on Scales" width="200px"/>
</p>

---

