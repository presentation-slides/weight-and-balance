## Cessna 172R

###### Our scenario

* VH-SCN
* Me (72kg) and Mary (65kg) front row
* Susan (67kg) rear seat
* Fuel 43 USG (_max: 56 USG = 212.8 L)_
* Baggage A 28kg
* Baggage B 10kg

---

## Cessna 172R

###### Important Note

* Unusable fuel is **included** in an aircraft Basic Empty Weight
* The Cessna 172R has 3 USG of unusable fuel
* This is significant and we will consider it this time

---

## Cessna 172R

###### Let's Do Our Weight and Balance

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### VH-SCN

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/c172r-scn-arm.png" alt="Cessna 172R VH-SCN Arm" width="550px" style="border: 4px solid #555"/>
</p>

---

## Cessna 172R

###### Add our known values

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-2.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Units of Measurement

* We will normalise our units of measurement using the Conversion Tables to:
  * Litres
  * kilograms (kg)
  * millimetres (mm)
* However, the flight manual may provide parameters in other units of measurement
* We will convert and record these other units of measurement on our W/B sheet
* This is to both minimise and detect error

---

## Cessna 172R

###### Convert our weights

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-3.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Add up our weights

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-4.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### We are over MTOW!

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-5.drawio.svg" alt="Cessna 172R Weight Grid" width="400px"/>
</p>

<div style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-certificated-weights.png" alt="Cessna 172R Certificated Weights" width="500px" style="border: 4px solid #555"/>
</div>

---

## Cessna 172R

###### We are over MTOW!

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-5.drawio.svg" alt="Cessna 172R Weight Grid" width="400px"/>
</p>

* However, we are under Maximum Ramp Weight (2457 lb / 1116.82 kg)
* Although we are **legal** with respect to weight, we will take this into consideration
* We can take no more weight
* _"Your small extra bag will need to stay behind, sorry"_

---

## Cessna 172R

###### We are under baggage weight limits

* Baggage Area A
  <span style="color: white; background-color: #2f4f4f; padding: 1px; font-size: x-large">
  &leq; `120 lb (54.4 kg)`
  </span>


* Baggage Area B
  <span style="color: white; background-color: #2f4f4f; padding: 1px; font-size: x-large">
    &leq; `50 lb (22.7 kg)`
  </span>


* Baggage Area A + Baggage Area B
  <span style="color: white; background-color: #2f4f4f; padding: 1px; font-size: x-large">
    &leq; `120 lb (54.4 kg)`
  </span>

---

## Calculating Weight Limitations

###### We are within **weight** limitations _(but only just)_

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/tick.png" alt="Tick" width="20px"/>
  <br/>
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/aeroplane-scales.png" alt="Aeroplane on Scales" width="200px"/>
</p>

---

## A Note on Baggage

* It is a legal requirement to restrain any baggage on **all flights**
* That restraint must meet certain strength and other parameters
* Civil Aviation Order 20.16.2

---

