## Cessna 172R

###### Balance

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/aeroplane-balance-pyramid.png" alt="Aeroplane Balance on Pyramid" width="350px"/>
</p>

---

## Cessna 172R

###### Balance

* There are two methods to calculate moment
  * The Cessna 172R flight manual provides the station **arms** in inches (`M = W x A`)
  * The flight manual also provides a **linear graph** to calculate the moment for each arm
* We can calculate moment using one method, then *cross-check* against the other
* Today, we will use the first method

---

## Cessna 172R

###### Station Arms

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-arms-1.png" alt="Cessna 172R arms" width="500px" style="border: 4px solid #555"/>
</p>

---

## Cessna 172R

###### Station Arms

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-arms-2.png" alt="Cessna 172R arms" width="500px" style="border: 4px solid #555"/>
</p>

---

## Cessna 172R

###### Linear Moment Graph

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-arms-3.png" alt="Cessna 172R moment graph" width="380px" style="border: 4px solid #555"/>
</p>

---

## Cessna 172R

###### Balance

* Furthermore, the flight manual provides more than one arm for some stations:
  * forward/mid/aft for both baggage areas
  * forward/mid/aft for row 1 crew
* We will calculate moment using both the forward and aft points for each station

---

## Cessna 172R

###### Balance

* Record each **forward arm** station from the flight manual
* Also record the stations that have only one arm provided _(no forward/aft movement)_
* Make it very clear to yourself that we are calculating a forward CG limit in this case

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-6.drawio.svg" alt="Cessna 172R Weight Grid" width="750px"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="font-size: x-large">Convert inches to millimetres _(multiply by `25.4`)_</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-7.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">Moment = Weight x Arm</span>
</p>

---

## Cessna 172R

###### Balance

* Calculate the moment for each station
* Remember to multiply the correct columns!

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-8.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="font-size: x-large">Add up the moments for ZFW and planned fuel</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-9.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">Arm = Moment &#247; Weight</span>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="font-size: x-large">Calculate the arm (mm) for ZFW and total weight</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-10.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Balance

* We have calculated the arm (in millimetres) at the forward limit
  * Front row seats all the way forward
  * Baggage placed at the forward limit of each baggage area
* Now let's do the same for front row seats and baggage at the **aft** limits

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="font-size: x-large">Add the arms for each station at their aft limits</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-11.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="font-size: x-large">Convert inches to millimetres</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-12.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="font-size: x-large">Calculate the moments for each station</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-13.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="font-size: x-large">Add the moments to get the ZFW and total moment</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-14.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="font-size: x-large">Arm = Moment &#247; Weight</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-15.drawio.svg" alt="Cessna 172R Weight Grid" width="800px"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="font-size: x-large">Summary</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/weight-grid-c172r-16.drawio.svg" alt="Cessna 172R Weight Grid" width="500px"/>
</p>

---

## Cessna 172R

###### Balance

* The Cessna 172R includes two different grids for plotting our result against CG limits
  * One grid plots weight against moment
  * The other plots weight against arm
* We could use either to find our limits
* We will use both

---

## Cessna 172R

###### Balance

* Each grid (or _envelope_) will contain four of our points:
  * (ZFW forward weight, ZFW forward moment)
  * (ZFW aft weight, ZFW aft moment)
  * (Total forward weight, Total forward moment)
  * (Total aft weight, Total aft moment)

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-weight-moment.png" alt="Cessna 172R Weight against Moment" width="420px" style="border: 4px solid #555"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-weight-moment-2.png" alt="Cessna 172R Weight against Moment" width="420px" style="border: 4px solid #555"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-weight-moment-3.png" alt="Cessna 172R Weight against Moment" width="420px" style="border: 4px solid #555"/>
</p>

---

## Cessna 172R

###### Balance

* We are within CG limits in the Normal Category
  * through the fuel burn
  * regardless of the position of row 1 seats and baggage within baggage area

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-weight-moment-4.png" alt="Cessna 172R Weight against Moment" width="350px" style="border: 4px solid #555"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <span style="font-size: x-large">Let's cross-check on the Weight vs Arm grid</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-weight-arm.png" alt="Cessna 172R Weight against Arm" width="350px" style="border: 4px solid #555"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-weight-arm-2.png" alt="Cessna 172R Weight against Arm" width="400px" style="border: 4px solid #555"/>
</p>

---

## Cessna 172R

###### Balance

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-weight-arm-3.png" alt="Cessna 172R Weight against Arm" width="400px" style="border: 4px solid #555"/>
</p>

---

## Balance

###### We are within balance limitations

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/tick.png" alt="Tick" width="20px"/>
  <br/>
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/aeroplane-balance-pyramid.png" alt="Aeroplane Balance on Pyramid" width="350px"/>
</p>

---

