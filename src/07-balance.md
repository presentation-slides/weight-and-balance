## Balance

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/aeroplane-balance-pyramid.png" alt="Aeroplane Balance on Pyramid" width="350px"/>
</p>

---

## Balance

* All aircraft have a Centre of Gravity (CG) at any given configuration
* All aircraft have both a **forward CG limit** and an **aft CG limit**
* We wish to ensure our aircraft is within these limits at all times
* We need to calculate the **balance** of the aircraft

---

## Balance

* Flying outside CG limits is both illegal and **extremely dangerous**
* If the aircraft is outside the forward CG limit, full-aft elevator **may not raise the nose**
* If the aircraft is outside the aft CG limit, full-forward elevator **may not push the nose down**
* This lesson is written in <span style="color: red; font-weight: bold">blood</span>

---

## Balance

<p style="text-align: center">
  <span style="font-size: x-large">11 May 2020, Rockhampton Airport</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/2020-05-11-vhhpe.png" alt="2020-05-11 VH-HPE" width="500px" style="border: 4px solid #555"/>
</p>


---

## Balance

###### Some basic principles of levers

* You are already familiar with levers &mdash; it's how your joints and muscles work
* You've also probably used a spanner to tighten a nut to a bolt
* The longer the spanner, the easier it is for you to tighten the nut
* In fact, if you double the length of the spanner, it is *twice as easy*

---

## Balance

###### Some basic principles of levers

* All aircraft have a specified CG position before any load is added _(empty weight CG)_
* This CG position is a distance measurement from the **reference datum**
* The reference datum may be thought of as the "zero position" from which all other CG measurements are taken
* You don't need to know exactly where the reference datum actually is, only that it exists

---

## Balance

###### Some basic principles of levers

* When we add load to an aircraft, the CG position changes
* We need to calculate that the CG position stays within the limits given our intended load
* This includes staying within limits **during fuel burn**

---

## Balance

###### Some basic principles of levers

* When we add `10kg` some distance from CG, then the CG position will change
* Adding `5kg` at *double the distance* from CG, will have the same effect
* Adding `20kg` at *half the distance* from CG, will also have the same effect
* This is called a *linear relationship*
  * between distance and effect on CG position
  * between weight and effect on CG position
  * if you create a graph of this relationship, you will see a **straight line**

---

## Balance

###### Some basic principles of levers

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/fulcrum-1.drawio.svg" alt="Fulcrum" width="800px"/>
</p>

---

## Balance

###### Some basic principles of levers

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/fulcrum-2.drawio.svg" alt="Fulcrum" width="800px"/>
</p>

---

## Balance

###### Some basic principles of levers

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/fulcrum-3.drawio.svg" alt="Fulcrum" width="800px"/>
</p>

---

## Balance

###### CG calculation

* Some terminology
* The parameters to a CG calculation include a **weight** and a distance called the **arm**
* The total effect caused by a given weight and arm is called **moment**
* In fact, given any two of these, we can easily determine the other

---

## Balance

###### CG calculation

<p style="text-align: center">
  <span style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">Moment = Weight x Arm</span>
</p>

* `Weight = Moment ÷ Arm`
* `Arm = Moment ÷ Weight`

---

## Balance

###### CG calculation

<p style="text-align: center">
  <span style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">Moment = Weight x Arm</span>
</p>

* If we apply weight `10kg` at a distance/arm of `200mm`
  * then we have created a moment
  * `10kg per 200mm`
  * or `2000 kg/mm`
* Similarly, if we apply weight `5kg` at a distance/arm of `400mm`
  * then we have created the same moment _(do the maths!)_

---

## Balance

###### CG calculation

* Aircraft include their empty CG position arm (from the reference datum) in the flight manual
* Sometimes the empty moment is also included, since it is the empty weight multipled by the empty CG arm

---

## Balance

###### Cessna 172R VH-SCN Arm and Moment

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/cessna172r-scn-arm.png" alt="Cessna 172R VH-SCN Arm" width="500px" style="border: 4px solid #555"/>
</p>

---

## Balance

###### Eurofox 24-4844 Arm

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-4844-arm.png" alt="Eurofox 4844 Arm" width="500px" style="border: 4px solid #555"/>
</p>

---

## Balance

###### Eurofox 24-8881 Arm and Moment

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-arm.png" alt="Eurofox 8881 Arm" width="500px" style="border: 4px solid #555"/>
</p>

---

## Balance

###### Calculating balance limitations

* Name the two new columns
* Enter the empty CG arm

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-arm2.drawio.svg" alt="Eurofox 8881 Arm" width="500px"/>
</p>

---

## Balance

###### Calculating balance limitations

* The arms for each station are provided in the flight manual
* Enter those as well

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-arm3.png" alt="Eurofox 8881 Arm" width="500px" style="border: 4px solid #555"/>
</p>

---

## Balance

###### Calculating balance limitations

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-arm4.drawio.svg" alt="Eurofox 8881 Arm" width="500px"/>
</p>

---

## Balance

###### Calculating balance limitations

* Calculate the **moment** for each station
* Hmm... how do we do that again?

---

## Balance

###### Calculating balance limitations

<p style="text-align: center">
  <span style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">Moment = Weight x Arm</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-arm5.drawio.svg" alt="Eurofox 8881 Arm" width="500px"/>
</p>

---

## Balance

###### Calculating balance limitations

* Add up the moments at each station to ZFW
* Add the Fuel moment to the ZFW moment to get total moment

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-arm6.drawio.svg" alt="Eurofox 8881 Arm" width="500px"/>
</p>

---

## Balance

###### Calculating balance limitations

* We now need to calculate the **ZFW arm** and the **total arm**
* Do we have a way to do this?

---

## Balance

###### Calculating balance limitations

<p style="text-align: center">
  <span style="color: white; background-color: #2b2d2f; padding: 5px; border-radius: 5px; font-weight: bold">Arm = Moment &#247; Weight</span>
</p>

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-arm7.drawio.svg" alt="Eurofox 8881 Arm" width="500px"/>
</p>

---

## Balance

###### Calculating balance limitations

* Are we within CG limits?
* All through the fuel burn?

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-arm8.drawio.svg" alt="Eurofox 8881 Arm" width="500px"/>
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-cg-range.png" alt="Eurofox 8881 CG Range" width="500px" style="border: 4px solid #555"/>
</p>

---

## Balance

###### We are within balance limitations

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/tick.png" alt="Tick" width="20px"/>
  <br/>
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/aeroplane-balance-pyramid.png" alt="Aeroplane Balance on Pyramid" width="350px"/>
</p>

---

## Balance

###### Considerations

* In the Eurofox 3K, burning fuel moves CG position **forward**
* In our example, we are not close to CG limits
* However, if we were say, near aft limit, we would expect to use more forward elevator in level flight
* Note: Eurofox 8881 has `85L` of usable fuel, `86L` total, so we can't burn that last litre anyway!

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/weight-and-balance/eurofox-8881-arm8.drawio.svg" alt="Eurofox 8881 Arm" width="500px"/>
</p>

---

## Balance

###### Considerations

* More complex aircraft have fuel tanks in the nose and also aft position &rarr; **CG considerations during flight**
* The more aft the CG position, the shorter the tail moment arm &rarr; **less longitudinal stability**
* The more forward the CG position, the longer the tail moment arm &rarr; **more longitudinal stability**
* Remember longitudinal stability from your Straight and Level lesson?

---

