## Units of Measurement

* You may have noticed that some aircraft use pounds (lb) while others use kilograms (kg) for weight
* Units of measurement are the **most common source of error** in calculating Weight and Balance
* We will use a specific technique to minimise this source of error

---

## Units of Measurement

###### Conversion Tables

* Conversion Tables for different units of measurement can be found in AIP GEN 2.6
* This includes fuel weight conversions to and from volume and weight

---

## Units of Measurement

###### AIP GEN 2.6 General Conversion Tables

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/aip-excerpt/general-conversions.png" alt="Fuel Volume/Weight Conversion Tables" width="350px"/>
</p>

---

## Units of Measurement

###### AIP GEN 2.6 Fuel Volume/Weight Conversion Tables

<p style="text-align: center">
  <img src="https://presentation-slides-media.gitlab.io/aip-excerpt/fuel-weight-conversion.png" alt="Fuel Volume/Weight Conversion Tables" width="500px"/>
</p>

---

